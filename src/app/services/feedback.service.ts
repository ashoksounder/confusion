import { Injectable } from '@angular/core';
import {Feedback} from '../shared/feedback';
import { Observable } from 'rxjs/Observable';
import {Http, Response} from '@angular/http';
import {baseURL} from '../shared/baseurl';
import {ProcessHttpmsgService } from './process-httpmsg.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { error } from 'util';
import { RestangularModule, Restangular } from 'ngx-restangular';

@Injectable()
export class FeedbackService {

  feedback:Feedback;
  value = null;

  constructor(private restangular: Restangular, private  processHttpmsgService: ProcessHttpmsgService) { }

  submitFeedback(feedback : Feedback): Observable<Feedback> {

          return this.restangular.all('feedback').post(feedback);
          
}
}
