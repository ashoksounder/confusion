import { Injectable } from '@angular/core';
import {Dish} from '../shared/dish';
import {DISHES} from '../shared/dishes';
import { Observable } from 'rxjs/Observable';
import {Http, Response} from '@angular/http';
import {baseURL} from '../shared/baseurl';
import {ProcessHttpmsgService } from './process-httpmsg.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { error } from 'util';
import { RestangularModule, Restangular } from 'ngx-restangular';

@Injectable()
export class DishService {

  value : any;

  constructor(private restangular:Restangular, private processHttpmsgService : ProcessHttpmsgService) { }
  getDishes(): Observable<Dish[]> {
    return this.restangular.all('dishes').getList();
  }

  getDishIds(): Observable<number[]>{
    return this.getDishes()
    .map(dishes => { return dishes.map(dish => dish.id) })
     .catch(error => { return this.processHttpmsgService.handleError(error); });;
  }

  getDish(id: number): Observable<Dish> {
    return this.restangular.one('dishes',id).get();
   
  } 

  getFeaturedDish(): Observable<Dish> {
    return this.restangular.all('dishes').getList({featured: true})
    .map(dishes => dishes[0]);
  }
}
 