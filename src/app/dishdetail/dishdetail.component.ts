import { Component, OnInit, Input , Inject} from '@angular/core';
import {Dish} from '../shared/dish';
import {DishService} from '../services/dish.service';
import {Params, ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import 'rxjs/add/operator/switchMap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Comment } from '../shared/comment';
import { flyInOut, visibility, expand } from '../animations/app.animation';


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.css'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
    animations: [
      flyInOut(),
      expand(),
     visibility()
  ]
})


export class DishdetailComponent implements OnInit {
  comment : Comment;
  commentform : FormGroup;

  dish : Dish;
  dishCopy = null;
  dishIds: number[];
  prev : number;
  next : number; 
  errMess : string; 
  visibility = 'shown';

  formErrors = {
    'author':'',
    'comment':''
  }

  validationMessages = {
    'author': {
      'required' : "User Name is required",
      'minlength' : "User Name should be minimum 2 chars long" 
    },
    'comment': {
      'required' : "Comment is required"
    }
  }

 
  constructor(private dishservice:DishService,
          @Inject('BaseURL') private BaseURL,
              private route:ActivatedRoute,
              private location:Location,
              private fb: FormBuilder) {
                this.createForm();
               }

  ngOnInit() {
    this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
    this.route.params
    .switchMap( (params:Params) =>{this.visibility='hidden'; return this.dishservice.getDish(+params["id"])})
    .subscribe(dish=>{ this.dish=dish;this.dishCopy = dish; this.setPrevNext(dish.id);this.visibility='shown';}, errmess=>{this.errMess=<any>errmess});
  }
  setPrevNext(dishId:number){
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
    }
  goBack():void{
  this.location.back();
  }

  createForm(){
    
      this.commentform = this.fb.group({
        author: ['',[Validators.required, Validators.minLength(2)]],
        comment: ['',[Validators.required]],
        rating: 3,
        date: [new Date().toISOString()]
      });
    
      this.commentform.valueChanges
         .subscribe(data=>this.onValueChanged(data));
      
      this.onValueChanged();
      }
    
      onValueChanged(data?: any){
        if(!this.commentform){return;}
        const form=this.commentform;
        for(const field in this.formErrors){
          this.formErrors[field] = '';
          const control = form.get(field);
          if(control && control.dirty && !control.valid){
           const messages = this.validationMessages[field];
           for (const key in control.errors){
             this.formErrors[field] += messages[key] + ' '; 
           }
          }
        }
      }
      onSubmit(){
        this.comment = this.commentform.value;
        this.dishCopy.comments.push(this.commentform.value);
        this.dishCopy.save()
                    .subscribe(dish=>this.dish=dish);
        console.log(this.dish);
        this.commentform.reset();
      }
    } 