import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Feedback, Contacttype } from '../shared/feedback';
import {FeedbackService} from '../services/feedback.service';
import { flyInOut, visibility, expand } from '../animations/app.animation';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  animations: [
    expand()
]
})
export class ContactComponent implements OnInit {

  feedback : Feedback;
  errMess : string;
  feedbackCopy : any;
  value = null;
  feedbackform : FormGroup;
  contacttype = Contacttype;
  formErrors = {
    'firstname':'',
    'lastname':'',
    'telnum':'',
    'email':''
  }

  validationMessages = {
    'firstname': {
      'required' : "First Name is required",
      'minlength' : "First Name should be minimum 2 chars long", 
      'maxlength' : "First Name should not exceed 25 chars", 
    },
    'lastname': {
      'required' : "First Name is required",
      'minlength' : "First Name should be minimum 2 chars long", 
      'maxlength' : "First Name should not exceed 25 chars", 
    },
    'telnum': {
      'required' : "First Name is required",
      'pattern' : "Telephone number shouldnot contain characters", 
    },
    'email': {
      'required' : "First Name is required",
      'email' : "Not a valid mail address", 
    }
  }
  constructor(private fb: FormBuilder, private feedbackservice: FeedbackService) {
     this.createForm();
   }

  ngOnInit() {
  
  }
  createForm(){

  this.feedbackform = this.fb.group({
    firstname: ['',[Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
    lastname: ['',[Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
    telnum: [0,[Validators.required, Validators.pattern]],
    email: ['', [Validators.required, , Validators.email]],
    agree: false,
    contacttype: 'None',
    message: ''
  });

  this.feedbackform.valueChanges
     .subscribe(data=>this.onValueChanged(data));
  
  this.onValueChanged();
  }

  onValueChanged(data?: any){
    if(!this.feedbackform){return;}
    const form=this.feedbackform;
    for(const field in this.formErrors){
      this.formErrors[field] = '';
      const control = form.get(field);
      if(control && control.dirty && !control.valid){
       const messages = this.validationMessages[field];
       for (const key in control.errors){
         this.formErrors[field] += messages[key] + ' '; 
       }
      }
    }
  }
  timer(){
    setTimeout(() => {
      this.feedbackCopy = null;
    }, 5000);
  }
  onSubmit(){
    this.feedbackCopy = this.feedbackform.value;
    this.feedbackservice.submitFeedback(this.feedbackCopy)
    .subscribe(feedback =>{ this.feedback=feedback;console.log(this.feedback)});
    this.feedbackform.reset();
  }
}
